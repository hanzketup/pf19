import React from "react"
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Button = styled.div`
  height: 3.5rem;
  width: 3.5rem;
  border-radius: 50%;
  background: rgba(255, 255, 255, 0.25);
  display: flex;
  justify-content: center;
  align-items: center;

  color: #fff;
  font-size: 2.4rem;
  cursor: pointer;
  opacity: 0.6;
  &:hover{opacity:0.9;}
  transition: 0.2s ease;

  position: fixed;
  top: 2.5rem;
  right: 15vw;

  @media (${x => x.theme.viewport.tablet}){top: 2.5rem; right: 4rem;}
  @media (${x => x.theme.viewport.phone}){top: 1.5rem; right: 1rem;}

  &::after{
    content: "${x => x.onesc ? 'or esc' : ''}";
    @media (${x => x.theme.viewport.tablet}){opacity:0;}
    position: absolute;
    left: 0;
    right: 0;
    bottom: -1.2rem;
    margin: 0 auto;
    font-size: 0.9rem;
    text-align: center;
  }

`

export default props =>
  <Button onesc={true} onClick={props.onClick}>
    <FontAwesomeIcon icon={['fal', 'times']} />
  </Button>
