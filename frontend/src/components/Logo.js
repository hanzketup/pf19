import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const LinkWrapper = styled(Link)`
  text-decoration: none;
`

const Title = styled.h1`
  position: relative;
  font-family: ${x => x.theme.fonts.marker};
  font-size: 2.5rem;
  line-height: 0.8;
  font-weight: 100;
  margin: 0;
  color: #ef5777;
  text-decoration: none !important;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
  transform: rotateZ(-4deg);

  @media (${x => x.theme.viewport.tablet}){
    //transform: rotateZ(0deg);
    margin-bottom: 1rem;
  }

  &::after{
    content: '----------';
    position: absolute;
    bottom: -1.2rem;
    left: 0;
    right: 0;
    margin: 0 auto;
    text-align: center;
    font-size: 2.4rem;
  }

`

export default props =>
  <LinkWrapper to='/'>
    <Title>
      Hannes <br />
      Hermansson!
    </Title>
  </LinkWrapper>
