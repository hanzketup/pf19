import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Navigation = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
`

const NavTo = styled(Link)`
  position: relative;
  font-family: ${x => x.theme.fonts.marker};
  font-size: 1.65rem;
  color: #ef5777;
  text-decoration: none;
  margin: 0 0.8rem;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
  opacity: 0.8;
  transform: rotate(-8deg);

  @media (${x => x.theme.viewport.tablet}){
    font-size: 1.4rem;
    transform: rotate(0deg);
    margin: 0 0.5rem;
  }

  &:hover{
    opacity: 1;
    text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.4), -1px -1px 3px rgba(0, 0, 0, 0.1);

    &::after{
      content: '___';
      font-family: ${x => x.theme.fonts.marker};
      position: absolute;
      bottom: -0.5rem;
      left: 0;
      right: 0;
      margin: 0 auto;
      text-align: center;
      font-size: 1.2rem;
    }
  }
`

const Dot = styled.b`
  font-size: 1.8rem;
  color: #ef5777;
  opacity: 0.5;

  @media (${x => x.theme.viewport.tablet}){
    font-size: 1.4rem;
    margin-top: -0.2rem;
  }

`

export default props =>
  <Navigation>
    <NavTo to='/'>Home</NavTo>
    <Dot>·</Dot>
    <NavTo to='/work'>Work</NavTo>
    <Dot>·</Dot>
    <NavTo to='/contact'>Contact</NavTo>
  </Navigation>
