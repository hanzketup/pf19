import React from "react"
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Wrapper = styled.div`
  position: sticky;
  top: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  margin-right: 4vw;

  @media (${x => x.theme.viewport.tablet}){
    position: relative;
    margin-right: 0;
    margin-bottom: 1rem;
    width: 100%;
  }

`

const Title = styled.div`
  text-align: center;
  position: relative;
  font-family: 'Permanent Marker',cursive;
  font-size: 1.8rem;
  color: #474747;
  -webkit-text-decoration: none;
  text-decoration: none;
  text-shadow: 1px 1px 3px rgba(0,0,0,0.1);
  opacity: 0.98;
  margin: 1rem 0 0 0;
  @media (${x => x.theme.viewport.phone}){
    font-size: 1.2rem;
    text-align: center;
    margin: 0 1rem;
  }
`

const Item = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 15rem;
  height: 3rem;
  padding: 0 0.8rem;
  margin: 0.15rem 0;
  background: rgba(0, 0, 0, 0.06);
  border-radius: 4px;
  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
  transition: 0.3s ease;
  cursor: pointer;
  color: #474747;
  text-decoration: none;
  &:hover{
    background: rgba(0, 0, 0, 0.1);
  }
 > p {
   display: flex;
   justify-content: center;
   align-items: center;
   margin: 0;
   line-height: 0.95;
   &::before{
      content: '';
      display: block;
      height: 0.8rem;
      width: 0.8rem;
      margin-top: 1px;
      margin-right: 1rem;
      border-radius: 50%;
      background-color: ${x => x.color};
    }
  }

  @media (${x => x.theme.viewport.tablet}){
    width: 95%;
  }

`

export default props =>
  <Wrapper>
    <Title>Sections</Title>
    {props.items.map(x =>
      <Item href={`#${x.name.replace(/ /g,"-")}`} color={x.color}>
        <p>{x.name}</p>
      </Item>
    )}
  </Wrapper>
