import React from 'react'
import styled, { keyframes } from 'styled-components'
import ReactMarkdown from 'react-markdown'

const pulse = keyframes`
  0% { opacity: 0.4; }
  50% { opacity: 1; }
  100% { opacity: 0.4; }
`

const Wrapper = styled.div`
  height: ${x => x.spacer || '20vh'};
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Letter = styled.h1`
  position: relative;
  font-family: ${x => x.theme.fonts.marker};
  font-size: 4.5rem;
  line-height: 0.8;
  font-weight: 100;
  margin: 0;
  color: #bdc3c7;
  text-decoration: none !important;
  text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
  transform: rotateZ(-4deg);
  animation: ${pulse} 0.5s infinite ease-in-out;

  @media (${x => x.theme.viewport.tablet}){
    //transform: rotateZ(0deg);
    margin-bottom: 1rem;
  }

`

export default props =>
  <Wrapper {...props}>
    <Letter>H</Letter>
  </Wrapper>
