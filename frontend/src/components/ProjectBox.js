import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Box = styled(Link)`
  display: flex;
  padding: 0.4rem 0.4rem;
  border-radius: 5px;
  color: #fff;
  opacity: 0.92;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15), -1px -1px 4px rgba(0, 0, 0, 0.15);
  cursor: pointer;
  transition:  0.6s ease;
  background: ${x => x.color};
  transform: perspective(1px) translateZ(0);
  backface-visibility: hidden;
  text-decoration: none;

  &:hover{
    transform: scale(1.05);
    opacity: 1;
    transition: color 0.6s ease, transform 0.3s ease;
  }

  @media (${x => x.theme.viewport.tablet}){
  }

  @media (${x => x.theme.viewport.phone}){
  }

`

const InnerBox = styled.div`
  position: relative;
  height: 18rem;
  width: 14rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  padding: 0.4rem;
  padding-bottom: 1rem;
  border-radius: 3px;
  border: 1px solid rgba(255, 255, 255, 0.4);

  &::before{
    content: 'PROJECT';
    position: absolute;
    left: -1.92rem;
    bottom: 3rem;

    font-size: 0.75rem;
    line-height: 0.8;
    padding: 0 0.3rem;
    transform: rotate(-90deg);
    background: ${x => x.color};
  }

`

const ProjectImage = styled.img`
  width: 12rem;
  height: 12rem

  padding: 0.4rem;
  border: 1px solid rgba(255, 255, 255, 0.3);
  border-radius: 50%;
  margin: 1rem;
  margin-top: 0rem;
  background: ${x => x.color};
`

const ProjectSummary = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto 0.2rem auto;

  > h2{
    font-size: 1.2rem;
    font-weight: normal;
    line-height: 1.1;
    margin: 0 0 0 0.1rem;
    text-align: center;
    transform: perspective(1px) translateZ(0);
    backface-visibility: hidden;
  }

  > h4{
    font-size: 0.7rem;
    font-weight: normal;
    margin: 0.3rem 0 0 0;
    text-align: center;
    transform: perspective(1px) translateZ(0);
    backface-visibility: hidden;
  }

`

export default props =>
  <Box color={props.color} to={props.to}>
    <InnerBox color={props.color}>
      <ProjectImage color={props.color} src={props.image} alt={props.title + ' screenshot'} />
      <ProjectSummary>
        <h2>{props.title}</h2>
        <h4>{props.type}</h4>
      </ProjectSummary>
    </InnerBox>
  </Box>
