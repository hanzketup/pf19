import React from "react"
import styled from 'styled-components'

const Wrapper = styled.div`
  margin: 0;
  white-space: pre-line;
`

const Title = styled.h1`
  position: relative;
  color: #ef5777;
  font-family: ${x => x.theme.fonts.marker};
  font-size: 2.2rem;
  text-align: center;
  font-weight: lighter;
  line-height: 1;
  margin: 1rem 0 0.0rem 0;

  &::after{
    content: '_ _ _';
    width: 100%;
    position: absolute;
    right: 0;
    bottom: -0.7rem;
    left: 0;
    text-align: center;
    margin: 0 auto;
  }

  @media (${x => x.theme.viewport.tablet}){
    font-size: 2.0rem;
  }

  @media (${x => x.theme.viewport.phone}){
    font-size: 1.8rem;
    opacity: 0.8;
  }

`

const Sub = styled.h3`
  color: #7f8c8d;
  font-family: ${x => x.theme.fonts.varela};
  font-size: 0.88rem;
  line-height: 0.6;
  margin: 0.8rem 0;
  letter-spacing: 1px;
  vertical-align: middle;
  text-align: center;
  font-weight: 100;

  @media (${x => x.theme.viewport.tablet}){
    font-size: 0.8rem;
  }

  @media (${x => x.theme.viewport.phone}){
    font-size: 0.72rem;
  }

`

export default props =>
  <Wrapper>
    <Title>{props.main}</Title>
    <Sub>{props.sub}</Sub>
  </Wrapper>
