import React from "react"
import styled from 'styled-components'
import { Field } from 'redux-form'

const InputWrap = styled.div`
border-radius: 5px;
background: #fff;

position: relative;
display: flex;
justify-content: center;
align-items: center;
box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15), -1px -1px 4px rgba(0, 0, 0, 0.15);
padding: 0.4rem;
margin: 0.6rem 0;

  > textarea{
    resize: vertical;
    height: 12rem;
    width: 40rem;
    border-radius: 3px;
    border: none;
    background: #fff;
    color: #474747;
    border: 1px solid rgba(0, 0, 0, 0.1);
    font-family: ${x => x.theme.fonts.text};
    font-size: 1.4rem;
    padding: 0.6rem;
    margin: 0;

  }

  @media (${x => x.theme.viewport.tablet}){
    width: 21rem;
    margin-left: auto;
    margin-right: auto;
  }

`

const FieldLabel = styled.label`
  position: absolute;
  top: 0rem;
  left: 1.4rem;
  color: rgba(0, 0, 0, 0.6);
  padding: 0 0.4rem;
  background: #fff;
  font-size: 0.8rem;
  margin: 0 auto;
`

const Req = styled.i`
  position: absolute;
  top: 0.4rem;
  right: 0.6rem;
  color: #ef5777;
  font-size: 1.8rem;
  font-style: normal;
`

export default props =>
  <InputWrap>
    <FieldLabel htmlFor={props.label}>{props.label}</FieldLabel>
    {props.required && <Req>*</Req>}
    <Field
      component="textarea"
      name={props.label}
      required={props.required}
      id={props.label} />
  </InputWrap>
