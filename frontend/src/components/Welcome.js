import React from "react"
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin-top: 2rem;
  margin-bottom: 1rem;

  @media (${x => x.theme.viewport.phone}){
    flex-direction: column;
  }
`

const Profile = styled.img`
  height: 240px;
  width: 240px;
  padding: 0.5rem;
  border-radius: 50%;
  margin-top: -0.5rem;
  box-shadow: 2px 2px 8px rgba(0, 0, 0, 0.1), -1px -1px 6px rgba(0, 0, 0, 0.08);

  @media (${x => x.theme.viewport.phone}){
    height: 180px;
    width: 180px;
  }

`

const Message = styled.div`
  margin-left: 5rem;

  > h1{
    text-align: left;
    position: relative;
    font-family: 'Permanent Marker',cursive;
    font-size: 3rem;
    color: #ef5777;
    -webkit-text-decoration: none;
    text-decoration: none;
    text-shadow: 1px 1px 3px rgba(0,0,0,0.1);
    opacity: 0.98;
    margin: 0;
    @media (${x => x.theme.viewport.phone}){
      font-size: 2rem;
      text-align: center;
      margin: 0 1rem;
    }
  }

  > p {
    font-size: 1.2rem;
    text-align: left;
    color: #6c757d;
    margin: 0.2rem 0 0 0;
    @media (${x => x.theme.viewport.phone}){
      font-size: 1rem;
      text-align: center;
      margin: 0 0.5rem;
    }
  }

  @media (${x => x.theme.viewport.phone}){
    margin-left: 0rem;
  }

`

const Bottom = styled.div`
  width: 100%;
  display: flex;
  justify-content:  flex-start;
  align-items: center;
  flex-wrap: wrap;
  padding-top: 0.1rem;
  @media (${x => x.theme.viewport.phone}){
    justify-content: center;
    padding: 0.2rem 1rem 1rem 1rem;
  }
`

const Option = styled.a`
  padding: 0.1rem 0.1rem -0.2rem 0.1rem;
  margin: 0.2rem 0.5rem;
  border-bottom: 3px dotted;
  border-color: ${x => x.color || "#eee"};
  border-radius: 0px;
  cursor: pointer;
  //box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.18), -1px -1px 4px rgba(0, 0, 0, 0.08);
  transition: 0.3s ease;
  color: #474747;
  text-decoration: none;
  opacity: 0.9;
  &:hover{
    opacity: 0.4;
  }
  &:first-child{
    margin-left: 0;
  }
`

export default props =>
  <Wrapper>
    <Profile src={require("../style/pfp.jpg")} />
    <Message>
      <h1>Hey, Nice to see you!</h1>
      <p>
      My name is Hannes and I'm a software developer from Sweden. <br />
      Here you'll find a showcase of some of my personal and professional projects. <br />
      Hop over to the contact tab to get in touch!
      </p>
      <Bottom>
      <Option href="mailto:me@hanneshermansson.com" target="_blank" color="#fab1a0">Email</Option>
      <Option href="https://www.linkedin.com/in/hannes-hermansson-35621b149/" target="_blank" color="#82ccdd">LinkedIn</Option>
      <Option href="https://www.instructables.com/member/HasseB/instructables/" target="_blank" color="#fdcb6e">Instructables</Option>
      <Option href="/cheatsheet" color="#82ddc1">Cheatsheet</Option>
      </Bottom>
    </Message>
  </Wrapper>
