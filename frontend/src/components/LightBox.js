import React, { Component } from "react"
import styled, { keyframes } from 'styled-components'

import Close from './Close'

const fadeIn = keyframes`
  0% { opacity: 0; }
  100% { opacity: 0.8; }
`

let Backdrop = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background: #fff;
  transition: 0.4s ease;
`

let Overlay = styled.div`
  height: 100%;
  width: 100%;
  opacity: 0.8;
  overflow-y: auto;
  background: ${x => x.color};
  animation: ${fadeIn} 1s ease;
`

class LightBox extends Component {

  componentDidMount(){
    this.esclistner = document.addEventListener('keydown', e => {
      if (e.keyCode === 27 && this.props.show) {
        this.props.closeAction()
      }
    })
  }

  componentWillUnmount(){
    document.removeEventListener('keydown', this.esclistner, true)
  }

  render() {
    return (
      <Backdrop show={this.props.show}>
        <Overlay color={this.props.color}>
          <Close onesc={true} onClick={this.props.closeAction} />
          {this.props.children}
        </Overlay>
      </Backdrop>
    )
  }
}


export default LightBox
