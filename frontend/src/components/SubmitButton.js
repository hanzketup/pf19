import React from "react"
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Button = styled.button`
  display: flex;
  background: #ef5777;
  padding: 0.4rem;
  border-radius: 5px;
  margin: 0.4rem;
  border: 0;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.15), -1px -1px 4px rgba(0, 0, 0, 0.15);
  margin-left: 30rem;
  opacity: 0.9;
  cursor: pointer;
  transition: 0.3s ease;

  &:hover{
    box-shadow: 1px 1px 3px rgba(0,0,0,0.4), -1px -1px 3px rgba(0,0,0,0.1);
    opacity: 1;
  }

  @media (${x => x.theme.viewport.tablet}){
    margin-left: auto;
  }

`

const ButtonInner = styled.div`
  position: relative;
  font-size: 2.2rem;
  color: rgba(255, 255, 255, 0.9);
  padding: 0.2rem 2.8rem;
  border-radius: 3px;
  border: 1px solid rgba(255, 255, 255, 0.6);

  &::after{
    content: 'Submit';
    position: absolute;
    top: -0.34rem;
    left: 0.6rem;
    color: #fff;
    padding: 0 0.4rem;
    background: #ef5777;
    font-size: 0.65rem;
    margin: 0 auto;
  }

`


export default props =>
  <Button type="submit">
    <ButtonInner>
      <FontAwesomeIcon icon={['fal', 'long-arrow-right']} />
    </ButtonInner>
  </Button>
