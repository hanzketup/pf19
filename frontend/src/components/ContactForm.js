import React, { Component } from "react"
import styled, { keyframes } from 'styled-components'
import { reduxForm } from 'redux-form'
import ReCAPTCHA from 'react-google-recaptcha'

import InputField from './InputField'
import TextField from './TextField'
import SubmitButton from './SubmitButton'

const pulse = keyframes`
  0% { opacity: 0.7; }
  50% { opacity: 1; }
  100% { opacity: 0.7; }
`

const Wrapper = styled.div`
  position: relative;
`

const Form = styled.form`
  opacity: ${x => x.hide ? "0" : "1"};
  ${x => x.waiting && `animation: ${pulse} 1s infinite ease-in-out`};
`

const Status = styled.div`
  position: absolute;
  top: 20%;
  left: 0;
  right: 0;
  font-family: ${x => x.theme.fonts.marker};
  color: #333;
  font-size: 2rem;
  line-height: 1;
  text-align: center;
  white-space: pre-wrap;
`

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {waiting: false, show_status: false, success: false};
  }

  async submitForm(ev) {
    ev.preventDefault();

    this.setState({waiting: true})

    const form = ev.target;
    const data = new FormData(form);

    let response = await fetch(form.action, {
      method: form.method,
      headers: {
        'Accept': 'application/json'
      },
      body: data
    })

    if(response.ok){
      this.setState({
        show_status: true,
        success: true,
        waiting: false
      })
    } else {
      this.setState({
        show_status: true,
        success: false,
        waiting: false
      })
    }

  }

  render() {
    return (
      <Wrapper>
        {this.state.show_status && <Status>{this.state.success ? "Thank you! \n I'll get back to you ASAP!" : "Uh Oh :( \n Something went wrong when dispatching the form, please try again!" }</Status>}
        <Form
          hide={this.state.show_status}
          waiting={this.state.waiting}
          onSubmit={this.submitForm.bind(this)}
          action="https://formspree.io/xpzrrjvp"
          method="POST">

          <InputField
            label="Name"
            type="text"
            required={true}
          />

          <InputField
            label="Email"
            type="email"
            required={true}
          />

          <TextField
            label="Message"
            required={true}
          />

          <SubmitButton />

        </Form>
      </Wrapper>
      )
  }
}


ContactForm = reduxForm({
    form: 'contact'
  })(ContactForm)

  export default ContactForm
