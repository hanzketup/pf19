import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Button = styled(Link)`
  font-size: 1.8rem;
  line-height: 0.9;
  padding: 0.4rem;
  border-radius: 5px;
  margin: 0.3rem 0.4rem;
  color: #fff;
  opacity: 0.8;
  box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.25), -1px -1px 4px rgba(0, 0, 0, 0.25);
  cursor: pointer;
  transition:  0.4s ease;
  background: ${x => x.color};
  text-decoration: none;

  > p{
    padding: 0.6rem 0.9rem;
    border-radius: 3px;
    background-color: rgba(255, 255, 255, 0.1);
    border: 0.12rem solid rgba(255, 255, 255, 0.2);
    box-shadow: 1px 1px 2px solid rgba(0, 0, 0, 0.2);
    margin: 0;
  }

  &:hover{
    opacity: 1;
    transform: scale(1.05);
    transition: color 0.6s ease, transform 0.3s ease;
  }

  @media (${x => x.theme.viewport.tablet}){
    font-size: 1.8rem;
  }

  @media (${x => x.theme.viewport.phone}){
    font-size: 1.4rem;
    font-weight: normal;
    border-radius: 4px;
    padding: 0.2rem;
    margin: 0.2rem 0.3rem;
    > p{
      padding: 0.4rem 0.6rem 0.4rem 0.6rem;
      border-radius: 2px;
    }
  }

`

export default props =>
  <Button color={props.color} to={props.to} >
    <p>{props.name}</p>
  </Button>
