import React from "react"
import styled from 'styled-components'
import { lighten, radialGradient } from 'polished'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import LightBoxContent from '../components/LightBoxContent'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  width: 50vw;
  min-height: 15rem;
  padding: 1rem 2rem;
  border-radius: 6px;
  margin: 0.4rem 0;
  box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.2), -2px -2px 3px rgba(0, 0, 0, 0.1);
  ${x => radialGradient({
    colorStops: [`${lighten(0.3, x.color)} 40%`, `${lighten(0.22, x.color)} 80%`],
    shape: 'ellipse',
  })}

  @media (${x => x.theme.viewport.tablet}){
    width: 95%;
    padding: 0.8rem 1rem;
  }

`

const Content = styled.div`

  hr{
    width: 80%;
    margin: 0 auto;
    border: 1px solid rgba(0, 0, 0, 0.07)
  }

  a{
    color: #474747;
    text-decoration: none;
    background: rgba(255, 255, 255, 0.4);
    padding: 0rem 0.3rem;
    border-radius: 3px;
    margin: 0.2rem 0;
    box-shadow: 1px 1px 4px rgba(0, 0, 0, 0.05);
    transition: 0.3s ease;
    &:hover{
      box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.2);
    }
  }
`

const Title = styled.div`
  text-align: left;
  position: relative;
  font-family: 'Permanent Marker',cursive;
  font-size: 1.8rem;
  color: #474747;
  -webkit-text-decoration: none;
  text-decoration: none;
  text-shadow: 1px 1px 3px rgba(0,0,0,0.1);
  opacity: 0.98;
  margin: 0;
  @media (${x => x.theme.viewport.phone}){
    font-size: 1.2rem;
    text-align: center;
    margin: 0 1rem;
  }
`

const Item = styled.a`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 15rem;
  height: 3rem;
  padding: 0 0.8rem;
  margin: 0.15rem 0;
  background: rgba(0, 0, 0, 0.06);
  border-radius: 4px;
  box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
  transition: 0.3s ease;
  cursor: pointer;
  &:hover{
    background: rgba(0, 0, 0, 0.1);
  }
 > p {
   display: flex;
   justify-content: center;
   align-items: center;
   margin: 0;
   &::before{
      content: '';
      display: block;
      height: 0.8rem;
      width: 0.8rem;
      margin-top: 1px;
      margin-right: 1rem;
      border-radius: 50%;
      background-color: ${x => x.color};
    }
  }

`

export default props =>
  <Wrapper color={props.color} id={props.name.replace(/ /g,"-")}>
    <Title>{props.name}</Title>
    <Content dangerouslySetInnerHTML={{ __html: props.content }} ></Content>
  </Wrapper>
