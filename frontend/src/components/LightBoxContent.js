import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 60%;
  padding: 6rem 0 0 0;
  margin: 0 auto;

  @media (${x => x.theme.viewport.tablet}){width: 90%;}
  @media (${x => x.theme.viewport.phone}){width: 92%;}

`

const Title = styled.h1`
  font-size: 3rem;
  font-weight: normal;
  color: rgba(255, 255, 255, 0.9);
  text-align: center;
  margin: 0;

`

const Body = styled.div`
  color: #fff;
  margin: 4rem 0 0 0;

  hr{
    width: 80%;
    margin: 0 auto;
    border: 1px solid rgba(0, 0, 0, 0.07)
  }

  a{
    color: #fff;
    text-decoration: none;
    background: rgba(0, 0, 0, 0.18);
    padding: 0rem 0.2rem;
    border-radius: 2px;
    margin: 0.2rem 0;
  }

`

export default props =>
  <Wrapper>
    <Title>{props.title}</Title>
    <Body dangerouslySetInnerHTML={{ __html: props.body }}>
    </Body>
  </Wrapper>
