let init = {
  show_lightbox: false,
  current_lightbox: {
    id: 0,
    name: '',
    color: '',
    body: ''
  },
  projects: [
    { id: 1, name: 'Discourse Sync', image: 'https://i.imgur.com/T4szs0c.jpg', type: 'Freelance', keyword: 'Node', color: '#43853d' },
    { id: 2, name: 'Discourse Sync', image: 'https://i.imgur.com/T4szs0c.jpg', type: 'Personal', keyword: 'Node', color: '#43853d' },
    { id: 3, name: 'Discourse Sync', image: 'https://i.imgur.com/T4szs0c.jpg', type: 'Personal', keyword: 'Node', color: '#43853d' },
  ]
}

export default (state = init, action) => {
  switch (action.type) {

    case "TOGGLE_PROJECT_LIGHTBOX":
      return {...state, show_lightbox: !state.show_lightbox}

    case "SWITCH_CURRENT_PROJECT_LIGHTBOX":
      let choosen_project = state.projects.filter(i => i.id === action.payload)[0]
      return {...state, current_lightbox: choosen_project}

    default:
      return state
  }
}
