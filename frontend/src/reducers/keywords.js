let init = {
  show_lightbox: false,
  current_lightbox: {
    id: 0,
    name: '',
    color: '',
    body: ''
  },
  words: [
    { id: 1, name: 'Web', color: '#10ac84' },
    { id: 2, name: 'Design', color: '#e74c3c' },
    { id: 3, name: 'Dev Ops', color: '#ff9f43' },
    { id: 4, name: 'IoT', color: '#8e44ad' },
    { id: 5, name: 'Wordpress', color: '#0073AA' },
    { id: 6, name: 'Node', color: '#43853d' },
    { id: 7, name: 'Docker', color: '#2496ed' },
    { id: 8, name: 'Testing', color: '#f39c12' },
    { id: 9, name: 'CI/CD', color: '#22a6b3' },
    { id: 10, name: 'Django', color: '#44B78B' },
    { id: 11, name: 'IT', color: '#30336b' },
    { id: 12, name: 'Raspberry Pi', color: '#c51d4a' },
    { id: 13, name: 'Arduino', color: '#00979d' }
  ]
}

export default (state = init, action) => {
  switch (action.type) {

    case 'TOGGLE_KEYWORD_LIGHTBOX':
      return {...state, show_lightbox: !state.show_lightbox}

    case 'SET_KEYWORDS':
      return {...state, words: action.payload}

    case 'SWITCH_CURRENT_KEYWORD_LIGHTBOX':
      let choosen_word = state.words.filter(i => i.id === action.payload)[0]
      return {...state, current_lightbox: choosen_word}

    default:
      return state
  }
}
