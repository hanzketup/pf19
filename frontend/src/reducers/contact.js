let init = {
  data: {},
  success: false,
  failed: false,
  error: ''
}

export default (state = init, action) => {
  switch (action.type) {

    case "SET_CONTACT_DATA":
      return {...state, data: {...state.data, ...action.payload}}

    case "SET_CONTACT_SUCCESS":
      return {...state, success: action.payload}

    case "SET_CONTACT_FAILURE":
      return {...state, failed: action.payload}

    case "SET_CONTACT_ERROR":
      return {...state, error: action.payload}

    default:
      return state
  }
}
