import { combineReducers } from "redux"
import { reducer as formReducer } from 'redux-form'

import keywords from './keywords'
import projects from './projects'
import contact from './contact'

const rootReducer = combineReducers({
  keywords,
  projects,
  contact,

  form: formReducer
})

export default rootReducer
