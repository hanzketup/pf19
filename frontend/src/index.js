import React from 'react'
import { render } from 'react-dom'
import { ThemeProvider } from 'styled-components'
import DocumentTitle from 'react-document-title'
import { BrowserRouter  as Router, Route } from "react-router-dom"
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import ApolloClient from 'apollo-boost'
import { ApolloProvider } from 'react-apollo'

import reducer from './reducers/index'
import registerServiceWorker from './registerServiceWorker'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faTimes,
  faLongArrowRight
} from '@fortawesome/pro-light-svg-icons'
import {
  faPaperPlane
} from '@fortawesome/free-solid-svg-icons'
import {
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons'
import theme from './style/theme'
import './style/global.css'

import Home from './scenes/Home'
import Work from './scenes/Work'
import Contact from './scenes/Contact'
import KeyWordLightBox from './scenes/KeyWordLightBox'
import ProjectLightBox from './scenes/ProjectLightBox'
import Cheatsheet from './scenes/cheatsheet'

library.add(
  faTimes,
  faLongArrowRight,
  faLinkedinIn,
  faPaperPlane
)

const client = new ApolloClient({
  uri: `https://us-central1-pf19-f0683.cloudfunctions.net/graphql`,
})

const store = createStore(reducer, applyMiddleware(thunk))
window.store = store

const MainRouter = () =>
    <ThemeProvider theme={theme}>
      <DocumentTitle title="Hannes Hermansson!">
        <Router>
          <div>
            <Route exact path="/" component={Home} />
            <Route exact path="/work" component={Work} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/skill/:slug" component={KeyWordLightBox} />
            <Route exact path="/project/:slug" component={ProjectLightBox} />
            <Route exact path="/cheatsheet" component={Cheatsheet} />
          </div>
        </Router>
      </DocumentTitle>
    </ThemeProvider>

  render(
    <ApolloProvider client={client}>
      <Provider store={store}>
        <MainRouter />
      </Provider>
    </ApolloProvider>,
    document.getElementById('root')
  )
registerServiceWorker()
