export default {
  fonts: {
    marker: "'Permanent Marker', cursive",
    text: "Varela"
  },
  viewport: {
    "phone": "max-width: 425px",
    "tablet": "max-width: 768px",
    "desktop": "max-width: 1200px",
  }
}
