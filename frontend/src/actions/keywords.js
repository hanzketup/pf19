import axios from 'axios'
import { grapql_url } from '../constants/app'

export const fetch_keywords = () => {
  return async dispatch => {
    let response = await axios({
      url: grapql_url,
      method: 'post',
      data: {
        query: `
          {
            keywords {
              id
              name
              body
              color
              order
            }
          }
          `
      }
    })

    console.log(response.data)
    dispatch({type: 'SET_KEYWORDS', payload: response.data.data.keywords})
  }
}

export const toggle_keyword_lightbox = () => {
  return {type: 'TOGGLE_KEYWORD_LIGHTBOX'}
}

export const switch_and_toggle_keyword_lightbox = (id) => {
  return dispatch => {
    dispatch({type: 'SWITCH_CURRENT_KEYWORD_LIGHTBOX', payload: id})
    dispatch({type: 'TOGGLE_KEYWORD_LIGHTBOX'})
  }
}
