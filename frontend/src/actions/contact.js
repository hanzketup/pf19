

export const set_form_data = (data) => {
  return {type: "SET_CONTACT_DATA", payload: data}
}

export const dispatch_contact_request = (data) => {
  console.log(data)
  return dispatch => {
    fetch(`${process.env.REACT_APP_API_URL}/post_contact_form`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({...data})
    })

  }
}
