

export const toggle_project_lightbox = () => {
  return {type: "TOGGLE_PROJECT_LIGHTBOX"}
}

export const switch_and_toggle_project_lightbox = (id) => {
  return dispatch => {
    dispatch({type: "SWITCH_CURRENT_PROJECT_LIGHTBOX", payload: id})
    dispatch({type: "TOGGLE_PROJECT_LIGHTBOX"})
  }
}
