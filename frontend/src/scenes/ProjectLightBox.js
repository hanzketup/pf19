import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

import ProjectLightBox from '../containers/ProjectLightBox'

export default withRouter(props =>
  <ProjectLightBox slug={props.match.params.slug} closeAction={() => props.history.push('/work')} />
)
