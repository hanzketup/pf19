import React from "react"
import DocumentTitle from 'react-document-title'

import RacingStripes from '../components/RacingStripes'
import Welcome from '../components/Welcome'

import Header from '../containers/Header'
import KeyCloud from '../containers/KeyCloud'
import {Page, Content} from '../style/wrappers'

export default props =>
  <Page>
    <DocumentTitle title="Home | Hannes Hermansson!" />
    <Header />

    <Content>
      <Welcome />
      <KeyCloud />
    </Content>

    <RacingStripes />
  </Page>
