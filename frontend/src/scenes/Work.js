import React from "react"
import DocumentTitle from 'react-document-title'

import ProjectGrid from '../containers/ProjectGrid'
import PageTitle from '../components/PageTitle'
import RacingStripes from '../components/RacingStripes'
import Header from '../containers/Header'
import {Page, Content, Spacer} from '../style/wrappers'

export default props =>
  <Page>
    <DocumentTitle title="Work | Hannes Hermansson!" />
    <Header />

    <Content>
      <Spacer />
      <PageTitle
        main="My Projects!"
        sub={`
          Here you'll find a list of my projects. \n
          fresh out of the oven, made just for you!
        `}
      />
      <ProjectGrid />
    </Content>

    <RacingStripes />
  </Page>
