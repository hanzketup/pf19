import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

import KeyWordLightBox from '../containers/KeyWordLightBox'

KeyWordLightBox
export default withRouter(props =>
  <KeyWordLightBox slug={props.match.params.slug} closeAction={() => props.history.push('/')} />
)
