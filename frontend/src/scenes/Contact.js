import React from 'react'
import DocumentTitle from 'react-document-title'

import RacingStripes from '../components/RacingStripes'
import PageTitle from '../components/PageTitle'
import ContactMessage from '../containers/ContactMessage'
import Header from '../containers/Header'
import {Page, Content, Spacer} from '../style/wrappers'

export default props =>
  <Page>
    <DocumentTitle title='Contact | Hannes Hermansson!' />
    <Header />

    <Content>
      <Spacer />
      <PageTitle
        main='Send me a message!'
        sub={`
          To make sure i get your message, fill out the form below.\n
          You may also reach me at Me@HannesHermansson.com
        `}
      />
      <ContactMessage />
    </Content>

    <RacingStripes />
  </Page>
