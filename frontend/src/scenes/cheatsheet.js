import React from 'react'
import DocumentTitle from 'react-document-title'

import RacingStripes from '../components/RacingStripes'
import PageTitle from '../components/PageTitle'
import CheatsheetBlocks from '../containers/CheatsheetBlocks'
import Header from '../containers/Header'
import {Page, Content, Spacer} from '../style/wrappers'

export default props =>
  <Page>
    <DocumentTitle title='Cheatsheet | Hannes Hermansson!' />
    <Header />

    <Content>
      <Spacer />
      <CheatsheetBlocks />
    </Content>

    <RacingStripes />
  </Page>
