import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import LightBoxContent from '../components/LightBoxContent'
import LightBox from '../components/LightBox'

const GET_PROJECT = gql`
  query($slug: String!){
    project(slug: $slug){
      name
      type
      content
      thumb
    }
  }
`

class ProjectLightBox extends Component {

  render() {
    return (
      <Query query={GET_PROJECT} variables={{slug: this.props.slug}}>
        {({ loading, error, data }) => {
          if (loading) return null
          if (error) return `Error! ${error}`

          return (
            <LightBox
              show={true}
              color={data.project.type === "freelance" ? "#489AC1" : "#78A773"}
              closeAction={this.props.closeAction}>

              <LightBoxContent
                title={data.project.name}
                body={data.project.content}
              />

            </LightBox>
        )
     }}
    </Query>
    )
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectLightBox)
