import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Logo from '../components/Logo'
import Menu from '../components/Menu'
let Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 15%;
  width: 100%;
  padding: 1.8rem 5vw;

  @media (${x => x.theme.viewport.tablet}){
    height: auto;
    padding: 0.6rem 4vw 0.4rem 4vw;
    margin-bottom: 1rem;

    flex-direction: column;
    nav {margin-top: 1rem;}
  }

`

class Header extends Component {

  render () {
    return (
      <Wrapper>
        <Logo />
        <Menu />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({}, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)
