import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled, { keyframes } from 'styled-components'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import * as buzzActions from '../actions/keywords'
import Loader from '../components/Loader'
import KeyWordButton from '../components/KeyWordButton'

const fadeIn = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`

let Wrapper = styled.div`
  transition: 1s ease;
`

let Cloud = styled.div`
  width: 38rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  margin-top: auto;
  margin-top: 0.5rem;
  animation: ${fadeIn} 1s ease;

  @media (${x => x.theme.viewport.tablet}){
    width: 90%;
    margin: auto;
    margin-top: 3rem;
  }

  @media (${x => x.theme.viewport.phone}){
    margin-top: 0.8rem;
    margin-bottom: 0.5rem;
  }
`

let HelperText = styled.p`
  font-size: 0.7rem;
  text-align: center;
  color: rgba(0, 0, 0, 0.3);
  margin-top: 0.6rem;
  margin-bottom: auto;
`

const Title = styled.div`
  text-align: center;
  position: relative;
  font-family: 'Permanent Marker',cursive;
  font-size: 2.2rem;
  color: #474747;
  -webkit-text-decoration: none;
  text-decoration: none;
  text-shadow: 1px 1px 3px rgba(0,0,0,0.1);
  opacity: 0.98;
  margin: 1rem 0 0 0;
  @media (${x => x.theme.viewport.phone}){
    font-size: 1.6rem;
    text-align: center;
    margin: 0 1rem;
  }
`

const GET_BUZZWORDS = gql`
  query{
    keywords{
      name
      color
      slug
    }
  }
`

class BuzzCloud extends Component {

  render () {
    return (
      <Wrapper>
        <Title>What i do:</Title>
        <Query query={GET_BUZZWORDS}>
            {({ loading, error, data }) => {
              if (loading) return <Loader spacer={"20vh"} />
              if (error) return `Failed to load!`

              return (
                <Cloud>
                  {data.keywords.map(word =>
                      <KeyWordButton
                        key={word.id}
                        id={word.id}
                        name={word.name}
                        color={word.color}
                        to={`/skill/${word.slug}`}
                    />
                  )}
                </Cloud>
              )
           }}
     </Query>
     <HelperText>Click on a buzzword to read more.</HelperText>
   </Wrapper>
    )
  }

}

const mapStateToProps = state => ({
  state: state.keywords
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...buzzActions}, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuzzCloud)
