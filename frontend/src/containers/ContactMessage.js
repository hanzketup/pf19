import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import * as contactActions from '../actions/contact'
import ContactForm from '../components/ContactForm'

class ContactMessage extends Component {

  submit(values){
    // Captcha is executed
    window.captcha.execute()

    // Stick the data in the contact store,
    // awaiting recaptcha callback to actually dispatch
    this.props.actions.set_form_data(values)
    setTimeout(() => console.log(this.props.state.data), 1000)

  }

  render() {
    return (
      <ContactForm
        onSubmit={this.submit.bind(this)}
        captchaAction={this.props.actions.dispatch_contact_request}
        formData={this.props.state.data}
      />
    )
  }
}

const mapStateToProps = state => ({
  state: state.contact
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...contactActions}, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactMessage)
