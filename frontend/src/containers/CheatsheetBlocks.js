import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import styled from 'styled-components'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import CheatsheetLegend from '../components/CheatsheetLegend'
import CheatsheetSection from '../components/CheatsheetSection'

let Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-start;

  @media (${x => x.theme.viewport.tablet}){
    flex-direction: column;
    align-items: center;
  }
`

let BlockWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`

const GET_CHEATSHEETS = gql`
  query keyword{
    cheatsheets{
      name
      content
      color
    }
  }
`

class BuzzCloud extends Component {

  render () {
    return (
      <Query query={GET_CHEATSHEETS}>
          {({ loading, error, data }) => {
            if (loading) return null
            if (error) return `Error! ${error}`

            return (
              <Wrapper>
                <CheatsheetLegend items={data.cheatsheets} />
                <BlockWrap>
                  {data.cheatsheets.reverse().map(item =>
                    <CheatsheetSection {...item} />
                  )}
                </BlockWrap>
              </Wrapper>
            )
         }}
   </Query>
    )
  }

}

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BuzzCloud)
