import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import LightBoxContent from '../components/LightBoxContent'
import LightBox from '../components/LightBox'

const GET_BUZZWORD = gql`
  query($slug: String!){
    keyword(slug: $slug){
      name
      color
      content
    }
  }
`

class KeyWordContent extends Component {

  render() {
    return (
      <Query query={GET_BUZZWORD} variables={{slug: this.props.slug}}>
        {({ loading, error, data }) => {
          if (loading) return null
          if (error) return `Error! ${error}`

          return (
            <LightBox
              show={true}
              color={data.keyword.color}
              closeAction={this.props.closeAction}>

              <LightBoxContent
                title={data.keyword.name}
                body={data.keyword.content}
              />

            </LightBox>
        )
     }}
    </Query>
    )
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KeyWordContent)
