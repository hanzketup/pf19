import React, { Component } from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import styled, { keyframes } from 'styled-components'

import gql from 'graphql-tag'
import { Query } from 'react-apollo'

import * as projectActions from '../actions/projects'
import Loader from '../components/Loader'
import ProjectBox from '../components/ProjectBox'

const fadeIn = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`

let Grid = styled.div`
  display: grid;
  grid-template-rows: repeat(${x => Math.ceil(x.count / 4)}, 1fr);
  grid-template-columns: repeat(4, 1fr);
  grid-auto-flow: row dense;
  grid-gap: 2rem;
  margin-top: 2rem;
  grid-column: -1;
  animation: ${fadeIn} 3s ease;
  transition: height 4s ease;

  @media (${x => x.theme.viewport.desktop}){
    grid-template-columns: repeat(3, 1fr);
  }

  @media (${x => x.theme.viewport.tablet}){
    grid-template-columns: repeat(2, 1fr);
  }

  @media (${x => x.theme.viewport.phone}){
    grid-template-columns: repeat(1, 1fr);
  }

`

const GET_PROJECTS = gql`
  query {
    projects {
      name
      type
      slug
      thumb
    }
  }
`

class ProjectGrid extends Component {

  render() {
    return (
      <Query query={GET_PROJECTS}>
        {({ loading, error, data }) => {
          if (loading) return <Loader spacer={"100vh"} />
          if (error) return `Failed to load!`
          return (
          <Grid count={this.props.state.projects.length}>
            {shuffle(data.projects)
            .map(project =>
              <ProjectBox
                key={project.id}
                id={project.id}
                title={project.name}
                image={project.thumb}
                type={project.type}
                color={project.type === "freelance" ? "#489AC1" : project.type === "personal" ? "#78A773" : "#D14A6E" }
                to={`/project/${project.slug}`}
              />
            )}
        </Grid>
        )
      }}
    </Query>
    )
  }
}

const mapStateToProps = state => ({
  state: state.projects
})

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({...projectActions}, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectGrid)

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}
