const admin = require("firebase-admin")
const functions = require("firebase-functions")
const express = require("express")

admin.initializeApp()
const db = admin.firestore()

const { ApolloServer, gql } = require("apollo-server-express")

const typeDefs = gql`
  type Keyword {
    name: String
    color: String
    content: String
    slug: String
  }

  type Cheatsheet {
    name: String
    color: String
    content: String
  }

  type Project {
    name: String
    type: String
    color: String
    content: String
    slug: String
    thumb: String
  }

  type Query {
    keywords: [Keyword]
    keyword(slug: String!): Keyword

    projects: [Project]
    project(slug: String!): Project

    cheatsheets: [Cheatsheet]
  }
`

const resolvers = {
  Query: {
    keywords: async () => {
      const keywords = await db.collection('keywords').get()
      return keywords.docs.map(keyword => keyword.data())
    },
    keyword: async (parent, { slug }) => {
      console.log("this is the slug", slug)
      const keyword = await db.collection('keywords').where('slug', '==', slug).get()
      return keyword.docs[0].data()
    },
    projects: async () => {
      const projects = await db.collection('projects').get()
      return projects.docs.map(project => project.data())
    },
    project: async (parent, { slug }) => {
      const projects = await db.collection('projects').where('slug', '==', slug).get()
      return projects.docs[0].data()
    },
    cheatsheets: async () => {
      const cheatsheets = await db.collection('cheetsheets').get()
      return cheatsheets.docs.map(cheatsheet => cheatsheet.data())
    },
  }
}

const app = express()
const server = new ApolloServer({ typeDefs, resolvers })
server.applyMiddleware({ app, path: "/", cors: true })

exports.graphql = functions.https.onRequest(app)
